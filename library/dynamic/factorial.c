int factorial(int num)
{
	int factorial = 1;

	while (num > 0)
	{
		factorial = factorial * num;
		num = num - 1;
	}

	return factorial;
}
